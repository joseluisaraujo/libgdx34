package cat.epiaedu.damviod.balmes.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class JocTest implements ApplicationListener, InputProcessor {
	SpriteBatch batch;
	//Texture img;
	private float elapsedTime = 0;
	private boolean flip = false;
	TextureAtlas textureAtlas;
	private BitmapFont font;
	Animation animation;
	Rectangle sprite;
	public float speedo = 0.0f;
	public float maxspeedo = 200.0f;
	public boolean holdingSpeed = false;
	long  soundID;
	Sound sound;
	Sound soundRunnin;
	public float pitch;
	TextureRegion texture;
	public float speedx;
	@Override
	public void create () {
		Gdx.input.setInputProcessor(this);
		batch = new SpriteBatch();
		sprite = new Rectangle();
		font = new BitmapFont();
		font.setColor(Color.BLUE);
		//img = new Texture("badlogic.jpg");
		textureAtlas = new TextureAtlas(Gdx.files.internal("Atlas/sprites/atlas.atlas"));
		animation = new Animation(2/15f, textureAtlas.getRegions());

		sprite.x = 100;
		sprite.y = 100;
		speedx = 0;
		texture = new TextureRegion();
		sound = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
		sound.play();
		soundRunnin = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("TextX", "onResize");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	if(holdingSpeed){
		speedo+= 5.0f;

	}if(!holdingSpeed){
			speedo-= 5.0f;
		}
	if(speedo >maxspeedo){

		speedo = 200;
	}
		pitch = 0.5f+speedo/maxspeedo *0.5f;
		soundRunnin.setPitch(soundID, pitch);
		if(speedo< 0){
			speedo = 0.0f;
		}
		batch.begin();
		sprite.x += speedx;
		elapsedTime += Gdx.graphics.getDeltaTime();
		texture.setRegion((TextureRegion) animation.getKeyFrame(elapsedTime, true));
		texture.flip(flip, false);
		//batch.draw(texture, 0, 0);
		//batch.draw(img, 0, 0);
		batch.draw(texture,sprite.x, sprite.y);
		//batch.setColor(0,255,0,1);
		//batch.draw(img, 200, 300);
		font.draw(batch, "Speedo: "+speedo+"km/h",100,100);
		batch.end();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose () {
		batch.dispose();
		textureAtlas.dispose();
		sound.dispose();
		//img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		Float x = Gdx.input.getX() - sprite.getWidth()/2;
		Float y = Gdx.graphics.getHeight() - Gdx.input.getY() - sprite.getHeight()/2;
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			sprite.setPosition( sprite.x, y );
		}

		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			flip = true;

			speedx -=2.0f;
		}
		if(Gdx.input.isKeyPressed (Input.Keys.RIGHT)){
			speedx +=2.0f;
			flip = false;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			soundID = soundRunnin.play();


			holdingSpeed = true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		speedx = 0;
		//soundRunnin.stop();
		holdingSpeed = false;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Float x = Gdx.input.getX() - sprite.getWidth()/2;
		Float y = Gdx.graphics.getHeight() - Gdx.input.getY() - sprite.getHeight()/2;
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			sprite.setPosition( x, y );
		}


		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		//speedx = 0;
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
