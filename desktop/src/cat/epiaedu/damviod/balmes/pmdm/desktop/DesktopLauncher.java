package cat.epiaedu.damviod.balmes.pmdm.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.epiaedu.damviod.balmes.pmdm.JocTest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Aixo es el nom que es mostra adalt la finestra";
		config.fullscreen = false;
		new LwjglApplication(new JocTest(), config);
	}
}
